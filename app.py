from flask import Flask, request, abort, send_from_directory
from docxtpl import DocxTemplate

app = Flask(__name__)

@app.route('/', methods=['POST'])
def generate():
    if not request.json or not 'template' in request.json or not 'context' in request.json:
        abort(400)
    
    template = request.json['template']
    
    if template not in ["act", "report"]:
        abort(400)
    
    doc = DocxTemplate("templates/%s.docx" % (template))
    context = request.json['context']
    doc.render(context)
    doc.save("generated.docx")
    
    return send_from_directory("", "generated.docx")

if __name__ == '__main__':
    app.run(host='0.0.0.0')
